<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'oc_blog');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '987975');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

#define('CONCATENATE_SCRIPTS', false);
#define('WP_MEMORY_LIMIT', '128M');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tHW.no&k(}eH4Ps.mhwM,*^<(6Q -s~_KQ~M50?u!;4OZ/4s]kA-Squ3+(6k|qUN');
define('SECURE_AUTH_KEY',  '_VAQaL8O`qV8}<. ~EHv,L_a!O6i8-;xp8`t$<HsKa5PR3$A1wdMl]oNm3JsZla(');
define('LOGGED_IN_KEY',    ',zO)P<6a;PX#Mn |,Ylcod>-M;G]#(kwx)al*<n4;{|Ef%,6xCH)xl?nuWh`MFa*');
define('NONCE_KEY',        'DkI>M%l{TM?Z;8&]k{*`-!(B3b?n+}+.JtU~^ ,tK5-,6$+@4(*7L)-5+s5K4vDI');
define('AUTH_SALT',        'j(.?%)(@|u~bbr-^|.G|2!N*dt3|Ym#Lvl{aw~I$+kioG:fG/}v=VVrcXS%b:n|/');
define('SECURE_AUTH_SALT', 'TkQ0kH`k|d~,!a(bRzJ$5r(X|LMJl]L3skLy!!^G)1{M9J 68>+)y%M|r4?X%gz[');
define('LOGGED_IN_SALT',   'K;r(o}wn]b=0k)Z6O*rrcq )E]_JvgpMGvj<- 25S|Z$3|wstq]?J|lnH6<sO!<5');
define('NONCE_SALT',       'dN^PF1cMTmvh8YNNjL`0g!S^>Xb[!x}oupIsPa JnM.2}#W|l?S|!~kj+48?pjlW');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
