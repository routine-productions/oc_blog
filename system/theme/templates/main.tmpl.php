<!DOCTYPE html>
<html>

<head>

<?php _LIB ('jquery') ?>
<?php _LIB ('pseudohover') ?>
<?php _LIB ('smart-title') ?>

<?php _CSS ('main') ?>
<?php _JS ('main') ?>

<?php if ($content['sign-in']['done?']) { ?>
<?php _CSS ('admin') ?>
<?php _JS ('admin') ?>
<?php } ?>

<e2:head-data />
<e2:scripts-data />

</head>

<body>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KN7VSD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KN7VSD');</script>
<!-- End Google Tag Manager -->

<?php _T_FOR ('form-install') ?>
<?php _T_FOR ('form-login') ?>

<?php if ($content['engine']['installed?']): ?>
<?php _T ('author-menu') ?>
<?php _T ('layout'); ?>
<?php if (!$content['sign-in']['done?'] and !$content['sign-in']['necessary?']) { ?>
<a class="e2-visual-login nu" id="e2-visual-login" href="<?= $content['hrefs']['sign-in'] ?>" class="nu"><span class="i-login"></span></a>
<?php } ?>
<?php endif ?>

</body>

</html>

<!-- <?=$content['engine']['version-description']?> -->