<?php

/*

	1200 - 100%
	 200 - x
	 
	 (1200*16.6)/100
	 (630*7.83)/100

	*/

if (isset($_GET['url'])) 
	$url = $_GET['url'];

	$img_size = getimagesize($url);
	$img_width = $img_size[0];
	$img_height = $img_size[1];

$img = ImageCreateFromJPEG($url);
$img_logo = ImageCreateFromPNG('http://5oclick.ru/blog/logo.png');

	$w_src = (1200*16.6)/100;  //imagesx($img_logo);
	$h_src = (1200*7.83)/100;  //imagesy($img_logo);
	$w_dest = imagesx($img);
	$h_dest = imagesy($img);
	 
	$transfer_x = ($img_width*5)/100;
	$transfer_y = ($img_width*5)/100;
	 
	imagecopyresampled($img, $img_logo, $transfer_x, $transfer_y, 0, 0, $w_src, $h_src, $w_src, $h_src);

header("Content-type: image/jpeg");
$color = imagecolorallocate($img, 255, 255, 255);
$font = "PTSansBold.ttf";
$font_size = ($img_width*4.1)/100;

if (isset($_GET['title'])) 
	$text = urldecode($_GET['title']);

	$text_a = explode(' ', $text);
	$text_new = '';
	foreach($text_a as $word){
		$box = imagettfbbox($font_size, 0, $font, $text_new.' '.$word);
		if($box[2] > $img_width - ($img_width*16.7)/100){
			$text_new .= "\n".$word;
		} else {
			$text_new .= " ".$word;
		}
	}
	$text_new = trim($text_new);
	$box = imagettfbbox($font_size, 0, $font, $text_new);
	$height_box = $box[1] + $font_size;

$offset_top = ($img_height / 2 ) - ($height_box / 4);
$offset_left = ($img_width*4.1)/100;

imagettftext($img, $font_size, 0, $offset_left, $offset_top, $color, $font, $text_new);
imagejpeg($img, NULL, 100);

?>