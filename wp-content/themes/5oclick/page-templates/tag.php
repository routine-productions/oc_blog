<?php
/**
 * Template Name: Tag template
 * The template for displaying Tag pages
 *
 * @package WordPress
 * @subpackage 5oclick
 */

get_header();
get_template_part('parts/index-header');
?>
    <section class="section main-news">
        <div class="main-news_body">
            <article>
                <h3>Теги:</h3>

                <div class="clearfix"></div>
                <div class="tag-cloud">
                    <?php
                    wp_tag_cloud([
                        "smallest" => 8,
                        "largest" => 42,
                        "unit" => "pt",
                        "number" => 45,
                        "format" => "flat",
                        "separator" => "\n",
                        "orderby" => "name",
                        "order" => "ASC",
                        "exclude" => null,
                        "include" => null,
                        "topic_count_text_callback" => "default_topic_count_text",
                        "link" => "view",
                        "taxonomy" => "post_tag",
                        "echo" => true,
                    ]);
                    ?>
                </div>
            </article>
        </div>
    </section>
<?php
get_footer();
