<?php get_header() ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <main>
        <div class="Post-Header">
            <figure>
                <?= get_image() ?>
                <div class="main-img_grad"></div>
                <?php wp_nav_menu([
                    'container' => '',
                    'menu_class' => 'list-inline category-list',
                    'theme_location' => 'navigation',
                    'items_wrap' => '<ul class="%2$s">%3$s</ul>',
                ]) ?>
                <figcaption>
                    <h1 class="Post-Heading">
                        <?= get_the_title() ?>
                        <?= get_bages_new(get_the_ID()) ?>
                    </h1>

                    <div class="clearfix"></div>

                    <ul class="list-unstyled data-list">
                        <li><span class="author"><?php the_author_posts_link() ?>
                                , <?php the_author_meta("description"); ?></span></li>
                        <li><span class="date"><?= dateToRussian(the_date('j F')) ?></span> <span
                                class="readtime"><?= get_read_time(get_the_ID()) ?></span></li>
                    </ul>

                    <?php get_template_part("parts/tags") ?>
                </figcaption>
            </figure>
        </div>

        <article class="Post-Content">
            <?php the_content() ?>
        </article>

        <footer class="Post-Meta">
            <div class="Post-Author">
                <figure>
                    <?php $author_email = get_the_author_meta("email");
                    echo get_avatar($author_email, "75"); ?>
                </figure>
                <div class="Post-Author-About">
                    <div class="Author-Name"><?php the_author_posts_link() ?></div>
                    <div class="Author-Info"><?php the_author_meta("description") ?></div>
                </div>
            </div>

            <aside class="Post-Social">
                <div class="likely" style="float:left" data-url="<?php echo get_permalink($prev_post->ID); ?>"
                     data-title="<?php echo get_the_title(); ?>">
                    <div class="twitter">Твитнуть</div>
                    <div class="facebook">Поделиться</div>
                    <div class="vkontakte">Поделиться</div>
                    <div class="pinterest">Запинить</div>
                </div>


                <div class="Posts-Navigation">
                    <div class="Posts-Navigation-Prev"><?php next_post_link('%link'); ?></div>

                    <div class="Posts-Navigation-Ctrl">
                        <span><?php next_post_link('%link','&#x2190;'); ?></span>
                        <span class="Ctrl">ctrl</span>
                        <span><?php next_post_link('%link','&#x2192;'); ?></span>
                    </div>

                    <div class="Posts-Navigation-Next"><?php previous_post_link('%link'); ?></div>
                </div>


                <button class="Show-Comments">Комментарии (<?= get_comments_number() ?>)</button>
            </aside>
        </footer>
    </main>
<?php endwhile; endif; ?>


<div class="Disqus-Comments" id="comments">
    <div class="Inner">
        <?php if (comments_open() || get_comments_number()) comments_template(); ?>
    </div>
</div>

<?php
get_template_part('parts/subscription');
get_template_part('parts/popular_inner');
?>

<svg class="Visually-Hidden">
    <symbol id="vk" viewBox="0 0 284 162">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M138.9,161.3h17c0,0,5.1-0.6,7.7-3.4c2.4-2.6,2.3-7.5,2.3-7.5s-0.3-22.8,10.2-26.1
	c10.4-3.3,23.8,22,38,31.7c10.7,7.4,18.9,5.8,18.9,5.8l37.9-0.5c0,0,19.8-1.2,10.4-16.8c-0.8-1.3-5.5-11.5-28.2-32.6
	c-23.8-22.1-20.6-18.5,8.1-56.7c17.4-23.3,24.4-37.4,22.2-43.5c-2.1-5.8-14.9-4.3-14.9-4.3l-42.7,0.3c0,0-3.2-0.4-5.5,1
	c-2.3,1.4-3.8,4.6-3.8,4.6s-6.8,18-15.8,33.3c-19,32.3-26.6,34-29.7,32c-7.2-4.7-5.4-18.8-5.4-28.8c0-31.3,4.7-44.3-9.2-47.7
	c-4.6-1.1-8.1-1.9-19.9-2c-15.2-0.2-28.1,0-35.4,3.6c-4.9,2.4-8.6,7.7-6.3,8c2.8,0.4,9.2,1.7,12.6,6.3c4.4,6,4.2,19.3,4.2,19.3
	s2.5,36.8-5.9,41.4c-5.8,3.1-13.7-3.3-30.6-32.6c-8.7-15-15.2-31.6-15.2-31.6s-1.3-3.1-3.5-4.8c-2.7-2-6.6-2.6-6.6-2.6L9.3,7.4
	c0,0-6.1,0.2-8.3,2.8c-2,2.4-0.2,7.2-0.2,7.2s31.8,74.3,67.7,111.8C101.5,163.6,138.9,161.3,138.9,161.3L138.9,161.3z"/>
    </symbol>
    <symbol id="twitter" viewBox="0 0 273.4 222.2">
        <path d="M273.4,26.3c-10.1,4.5-20.9,7.5-32.2,8.8c11.6-6.9,20.5-17.9,24.7-31C255,10.5,243,15.2,230.2,17.7C220,6.8,205.4,0,189.3,0
	c-31,0-56.1,25.1-56.1,56.1c0,4.4,0.5,8.7,1.5,12.8C88,66.5,46.7,44.2,19,10.3c-4.8,8.3-7.6,17.9-7.6,28.2c0,19.5,9.9,36.6,25,46.7
	c-9.2-0.3-17.8-2.8-25.4-7c0,0.2,0,0.5,0,0.7c0,27.2,19.3,49.8,45,55c-4.7,1.3-9.7,2-14.8,2c-3.6,0-7.1-0.4-10.6-1
	c7.1,22.3,27.9,38.5,52.4,39c-19.2,15-43.4,24-69.7,24c-4.5,0-9-0.3-13.4-0.8c24.8,15.9,54.3,25.2,86,25.2
	c103.2,0,159.6-85.5,159.6-159.6c0-2.4-0.1-4.9-0.2-7.3C256.4,47.4,265.9,37.5,273.4,26.3z"/>
    </symbol>
    <symbol id="facebook" viewBox="0 0 117.7 226.6">
        <path id="f" d="M76.4,226.6V123.2h34.7l5.2-40.3H76.4V57.2c0-11.7,3.2-19.6,20-19.6l21.3,0v-36C114,1.1,101.3,0,86.6,0
	C55.8,0,34.8,18.8,34.8,53.2v29.7H0v40.3h34.8v103.4H76.4z"/>
    </symbol>
    <symbol id="plus" viewBox="0 0 32 32">
        <line fill="none" stroke="#fff" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="16" y1="29"
              x2="16" y2="3"/>
        <line fill="none" stroke="#fff" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="3" y1="16"
              x2="29" y2="16"/>
    </symbol>
</svg>


<div class="Social-Mobile">
    <ul class="Buttons-List">
        <li class="Icon Twitter">
            <a href="#">
                <svg>
                    <use xlink:href="#twitter"></use>
                </svg>
            </a>
        </li>
        <li class="Icon Facebook">
            <a href="#">
                <svg>
                    <use xlink:href="#facebook"></use>
                </svg>
            </a>
        </li>
        <li class="Icon Vk">
            <a href="#">
                <svg>
                    <use xlink:href="#vk"></use>
                </svg>
            </a>
        </li>
        <li class="Icon More">
            <a href="#">
                <svg>
                    <use xlink:href="#plus"></use>
                </svg>
            </a>
        </li>
    </ul>
</div>

<?php get_footer() ?>
