<div class="Post-Card">
    <figure>
        <?= get_image(true) ?>
        <figcaption><a href="<?= esc_url(get_permalink()) ?>"><h4><?php the_title() ?></h4></a></figcaption>
    </figure>
    <div class="Post-Meta">
        <span class="Post-Author"><?php the_author_posts_link() ?></span>
        <small class="Post-Read-Time"><?= get_read_time(get_the_ID()) ?></small>
    </div>
    <a href="<?= esc_url(get_permalink()) ?>"><?= get_card_content(get_the_content("")) ?></a>
    <footer>
        <a href="<?= esc_url(get_permalink()) ?>" class="Btn Read-More">Читать далее</a>

        <div class="Post-Social">
            <span class="Comments-Amount" style="margin-right: 20px">
                <svg viewBox="0 0 19 16">
                    <path d="M17,1c0.8,0,1,0.2,1,1v8c0,0.8-0.2,1-1,1H9H8.6l-0.3,0.3L5,14.6V12v-1H4H2c-0.8,0-1-0.2-1-1V2c0-0.8,0.2-1,1-1H17 M17,0H2
		C0.7,0,0,0.7,0,2v8c0,1.3,0.7,2,2,2h2v4c0.2,0,0.4,0,0.5,0c0.2,0,0.3,0,0.5,0l4-4h8c1.3,0,2-0.7,2-2V2C19,0.7,18.3,0,17,0L17,0z"/>
                </svg>
                <a href="<?= esc_url(get_permalink()) ?>#comments"><?= get_comments_number() ?></a>
            </span>

            <div class="likely" style="float:right;" data-url="<?php echo get_permalink($prev_post->ID); ?>"
                 data-title="<?php echo get_the_title(); ?>">
                <div class="twitter"></div>
                <div class="facebook"></div>
                <div class="vkontakte"></div>
                <div class="pinterest"></div>
            </div>
        </div>
    </footer>
    <?php get_template_part("parts/tags") ?>
</div>