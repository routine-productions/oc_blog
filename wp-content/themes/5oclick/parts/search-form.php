<div class="Search">
	<form action="<?php bloginfo("url"); ?>/" method="get" class="form-inline" role="form">
			<input type="text" name="s" value="<?= htmlspecialchars(get_search_query()) ?>" class="form-control" placeholder="что ищем?">
		<a href="#" class="Btn-Search">
			<svg viewBox="0 0 22 22"><path d="M21.1,18.9l-6.3-6.3C15.5,11.4,16,10,16,8.5C16,4.4,12.6,1,8.5,1C4.4,1,1,4.4,1,8.5S4.4,16,8.5,16c1.5,0,2.9-0.5,4.1-1.2
	l6.3,6.3c0.3,0.3,0.7,0.4,1.1,0.4s0.8-0.1,1.1-0.4C21.6,20.5,21.6,19.5,21.1,18.9z M4,8.5C4,6,6,4,8.5,4S13,6,13,8.5S11,13,8.5,13
	S4,11,4,8.5z"/></svg>
		</a>
	</form>
</div>
