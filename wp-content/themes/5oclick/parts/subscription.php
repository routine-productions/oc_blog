<div class="Subscription-Form">
    <h4>Получайте новые статьи</h4>

    <p>
        Сразу после выхода &mdash; в <a href="#">Телеграме</a><br>
        или раз в месяц &mdash; по почте.
    </p>

    <form action="#" method="post" class="Text-Centrate" style="text-align:center" role="form">
        <label class="sr-only" for="">E-mail</label>
        <input type="tel" class="form-control" id="" placeholder="example@gmail.com">
        <button type="submit" class="Btn Read-More">Подписаться</button>
    </form>
</div>