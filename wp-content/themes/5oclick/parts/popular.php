<?php if (empty($_COOKIE["hide_popular"])) { ?>
    <section class="News-Popular">
        <header>
            <h2>
                <a href="#">Популярное в блоге</a>
            </h2>

            <svg class="Icon Close" viewBox="0 0 22 22">
                <g fill="none" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10">
                    <line x1="2" y1="20" x2="20" y2="2"/>
                    <line x1="20" y1="20" x2="2" y2="2"/>
                </g>
            </svg>
        </header>

        <div class="Hide-Mobile">
            <h3>
                <a href="#">За месяц</a>
            </h3>
        </div>

        <?php
        $popularpost = new WP_Query([
            'posts_per_page' => 3,
            'date_query' => [[
                'after' => '-1 month',
                'inclusive' => true,
            ]],
            'meta_key' => 'wpb_post_views_count',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
        ]);
        while ($popularpost->have_posts()) {
            $popularpost->the_post();
            get_template_part('parts/card');
        }
        ?>

        <div class="Hide-Mobile">
            <h3>
                <a href="#">За все время</a>
            </h3>
        </div>



        <?php
        $popularpost = new WP_Query([
            'posts_per_page' => 3,
            'meta_key' => 'wpb_post_views_count',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
        ]);
        while ($popularpost->have_posts()) {
            $popularpost->the_post();
            get_template_part('parts/card');
        }
        ?>
    </section>
<?php } ?>
