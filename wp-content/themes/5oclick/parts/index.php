<section class="Post-Header">
    <figure>
        <?= get_image() ?>
        <div class="main-img_grad"></div>
        <figcaption>
            <h1 class="Post-Heading">
                <?= get_the_title() ?>
                <?= get_bages_new(get_the_ID()) ?>
            </h1>

            <div class="clearfix"></div>

            <ul class="list-unstyled data-list">
                <li><span class="author"><?php the_author_posts_link() ?>
                        , <?php the_author_meta("description") ?></span></li>
                <li><span class="date"><?= dateToRussian(the_date('j F')) ?></span> <span
                        class="readtime"><?= get_read_time(get_the_ID()) ?></span></li>
            </ul>

            <?php get_template_part("parts/tags") ?>
        </figcaption>
    </figure>

    <article><?php the_content("") ?></article>

    <footer>
        <a href="<?= esc_url(get_permalink()) ?>" class="Btn Read-More">Читать далее</a>

        <div class="Social-Links">
                        <span class="Comments-Amount" style="margin-right: 20px">
                            <a href="<?= esc_url(get_permalink()) ?>#comments">
                                <?php printf(_nx('1 комментарий', '%1$s комментариев ', get_comments_number(), 'comments title', 'twentyfifteen'),
                                    number_format_i18n(get_comments_number()), get_the_title()); ?>
                            </a>
                        </span>
           <div class="likely" style="float:right;" data-url="<?php //echo get_permalink($prev_post->ID); ?>"
                 data-title="<?php //echo get_the_title(); ?>">
                <div class="twitter"></div>
               <div class="facebook"></div>
                <div class="vkontakte"></div>
                <div class="pinterest"></div>
           </div>
        </div>
    </footer>
</section>