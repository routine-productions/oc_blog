<section class="Main-Header">
        <div class="Left">
            <span class="logo"><a href="<?php bloginfo('url') ?>">Блог <?php bloginfo('name'); ?></a></span>

            <p><?php bloginfo('description'); ?></p>
            <?php wp_nav_menu([
                'container' => '',
                'menu_class' => 'list-inline category-list',
                'theme_location' => 'navigation',
                'items_wrap' => '<ul class="%2$s">%3$s</ul>',
            ]) ?>
        </div>

        <div class="Right">
            <ul class="share-list list-inline">
                <li>Подписаться</li>
                <li><a href="#"><i class="Icons Telegram"></i></a></li>
                <li><a href="#"><i class="Icons Facebook"></i></a></li>
                <li><a href="#"><i class="Icons Instagram"></i></a></li>
<!--                <li><a href="#"><i class="Icons More"></i></a></li>-->
            </ul>
            <?php get_template_part('parts/search-form'); ?>
        </div>
</section>
