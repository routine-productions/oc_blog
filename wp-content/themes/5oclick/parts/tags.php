<ul class="Tags-List">
	<?php
	$posttags = get_the_tags();
	if ($posttags) {
		foreach($posttags as $tag) {
			echo "<li><a href='/blog/tag/".$tag->slug."/' class='tag'>".$tag->name."</a></li>\n";
		}
	}
	?>
</ul>