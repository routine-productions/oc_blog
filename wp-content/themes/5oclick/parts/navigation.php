<section class="section cta-form">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navigation">
                    <div style="text-align:left"><?php next_posts_link('← Старые записи') ?></div>
                    <div style="text-align:right"><?php previous_posts_link('Новые записи →') ?></div>
                </div>
            </div>
        </div>
    </div>
</section>
