<section class="News-Latest">
    <h3>
        <a href="#">Последние статьи</a>
    </h3>

    <article>
        <?php latest_block(['posts_per_page' => 3, 'orderby' => 'post_date', 'order' => 'DESC']) ?>
    </article>

    <footer>
        <a href="javascript:;" class="Btn Load-More">Загрузить еще статьи</a>

        <div class="loader loader-posts">
            <img src="<?php bloginfo("template_url") ?>/img/loader.gif"/>
        </div>
    </footer>

</section>
