<footer class="footer"></footer>
</div>
<script type='text/javascript'>
    /* <![CDATA[ */
    var home = "<?= get_site_url() ?>";
    var ajaxpagination = {
        "ajaxurl": "<?= str_replace("/", "\\/", get_bloginfo("url")) ?>\/wp-admin\/admin-ajax.php"
    };
    /* ]]> */
</script>
<script src="<?php bloginfo("template_url"); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo("template_url"); ?>/js/likely.js"></script>
<script src="<?php bloginfo("template_url"); ?>/js/main.js"></script>
<script src="<?php bloginfo("template_url"); ?>/index.min.js"></script>
<?php wp_footer(); ?>

</body>
</html>
