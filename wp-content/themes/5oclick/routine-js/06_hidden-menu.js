(function ($) {
    // Toggle Search
    $('a.Search').click(function () {
        $('.Search-Popup').fadeIn(300);
        return false;
    });

    $('.Search-Popup .Close').click(function () {
        $('.Search-Popup').fadeOut(300);
    });

    $('.Search-Popup').click(function (Event) {
        if ($(Event.target).is('.Search-Popup')) {
            $('.Search-Popup').fadeOut(300);
        }
    });


    // Toggle - Menu
    $('a.Menu.Hamburger').click(function () {
        $('.Menu-Popup').fadeIn(300);

        $('.Menu-Popup-Content').css('left', '20%');
        return false;
    });

    $('.Menu-Popup .Close').click(function () {
        $('.Menu-Popup').fadeOut(300);
        $('.Menu-Popup-Content').css('left', '100%');
    });

    $('.Menu-Popup').click(function (Event) {
        if ($(Event.target).is('.Menu-Popup')) {
            $('.Menu-Popup').fadeOut(300);
            $('.Menu-Popup-Content').css('left', '100%');
        }
    });

    // Search Query
    $('.Search-Popup .Search').click(function () {
        var Search = $('.Search-Popup input').val();
        if (Search.length > 0) {
            location.href = '/?s=' + Search;
        }
        return false;
    });

    $('.Search-Popup input').keypress(function (e) {
        if (e.which == 13) {
            var Search = $('.Search-Popup input').val();
            if (Search.length > 0) {
                location.href = '/?s=' + Search;
            }
            return false;
        }
    });

    //
    // Search Query Desktop
    $('.Btn-Search').click(function () {
        if ($('.Search input.form-control').is(':visible')) {
            var Search = $('.Main-Header .Search input').val();
            if (Search.length > 0) {
                location.href = $('.Search form.form-inline').attr('action') + '?s=' + Search;
            }
            return false;
        }
    });
    // Search Desktop Show
    $('.Btn-Search').click(function () {
        if ($('.Search input.form-control').is(':hidden')) {
            $('.Search input.form-control').show();
        }
    });

    $('.Main-Header .Search input').keypress(function (e) {
        if (e.which == 13) {
            var Search = $('.Main-Header .Search input').val();
            if (Search.length > 0) {
                location.href = '/?s=' + Search;
            }
            return false;
        }
    });

    // Search Set
    var Search = '';
    var Vars = window.location.search.substring(1).split("&");
    for (var i = 0; i < Vars.length; i++) {
        var Pair = Vars[i].split("=");
        if (Pair[0] == 's') {
            Search = Pair[1];
        }
    }

    $('.Search-Popup input').val(decodeURIComponent(Search));

    // Hide News
    $('.News-Popular .Close').click(function () {
        $('.News-Popular').remove();
    });

    // Click to comments
    $('.Comments-Amount').click(function () {
        location.href = $(this).parents('.Post-Card').find('a').attr('href') + '#comments';
    });

    // Show comments
    if (location.hash == '#comments') {
        $('.Disqus-Comments').css('display', 'block');
        $('.Show-Comments').css('display', 'none');
    }

    $('.Show-Comments').click(function () {
        $('.Disqus-Comments').css('display', 'block');
        $('.Show-Comments').css('display', 'none');
    });

    // Article Navigation
    $(document).keydown(function (e) {
        if (e.keyCode == 37 && e.ctrlKey) {
            location.href = $('.Posts-Navigation-Prev a').attr('href');
        }
        if (e.keyCode == 39 && e.ctrlKey) {
            location.href = $('.Posts-Navigation-Next a').attr('href');
        }
    });


})(jQuery);
