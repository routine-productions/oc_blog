<?php
get_header();
get_template_part('parts/social-mobile');
get_template_part('parts/index-header');

while (have_posts()) {
    the_post();
    get_template_part('parts/index');
}

if (!is_admin() && is_home()) {
    get_template_part('parts/popular');
    get_template_part('parts/latest');
    get_template_part('parts/subscription');
    ?>
    <?php
}
if (!is_admin() && !is_home()) {
    get_template_part('parts/navigation');
}

get_footer();
