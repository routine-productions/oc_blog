(function($){
    $(".News-Popular .Icon.Close").on("click", function(){
        var date = new Date();
        date.setMonth(date.getMonth() + 1);console.log(date);
        document.cookie = "hide_popular=1; path=/; expires=" + date.toUTCString();
        $(this).parents(".pop-news").hide();
    });

    var page = 1;
    $(".Load-More").on("click", function(){
        event.preventDefault();
        if ("stop" == page) return;
        var obj = this;
        $.ajax({
            url: ajaxpagination.ajaxurl,
            type: "post",
            dataType: "json",
            data: {
                action: 'ajax_pagination',
                query_vars: ajaxpagination.query_vars,
                page: page
            },
            beforeSend: function() {
                $(obj).hide();
                $(".loader-posts").show();
            },
            success: function(response) {
                if (!response || !response.result || !response.html) {
                    page = "stop";
                    return;
                }
                $(response.html).appendTo(".News-Latest article");
                page++;
            },
            complete: function() {
                $(".loader-posts").hide();
                $(obj).show();
            }
        })
    });
})(jQuery);
