<!doctype html>
<html class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.png" type="image/x-icon">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/index.min.css">

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="app">
    <div class="Topbar">
        <div class="Inner">
            <a class="Menu Hamburger" href="#"><span></span></a>
            <a class="Logo" href="/"><img src="<?= get_site_url()?>/images/logo.png"></a>

            <a class="Search" href="#">
                <svg viewBox="0 0 22 22">
                    <path d="M21.1,18.9l-6.3-6.3C15.5,11.4,16,10,16,8.5C16,4.4,12.6,1,8.5,1C4.4,1,1,4.4,1,8.5S4.4,16,8.5,16c1.5,0,2.9-0.5,4.1-1.2
	l6.3,6.3c0.3,0.3,0.7,0.4,1.1,0.4s0.8-0.1,1.1-0.4C21.6,20.5,21.6,19.5,21.1,18.9z M4,8.5C4,6,6,4,8.5,4S13,6,13,8.5S11,13,8.5,13
	S4,11,4,8.5z"/>
                </svg>
            </a>

            <form class="Subscription">
                <label for="Email">Получайте полезный&nbsp;контент</label>
                <input type="email" id="Email" placeholder="example@gmail.com">
                <button>Подписаться</button>
            </form>

            <div class="Search-Popup">
                <svg class="Icon Search" viewBox="0 0 22 22">
                    <path d="M21.1,18.9l-6.3-6.3C15.5,11.4,16,10,16,8.5C16,4.4,12.6,1,8.5,1C4.4,1,1,4.4,1,8.5S4.4,16,8.5,16c1.5,0,2.9-0.5,4.1-1.2
	l6.3,6.3c0.3,0.3,0.7,0.4,1.1,0.4s0.8-0.1,1.1-0.4C21.6,20.5,21.6,19.5,21.1,18.9z M4,8.5C4,6,6,4,8.5,4S13,6,13,8.5S11,13,8.5,13
	S4,11,4,8.5z"/>
                </svg>

                <form method="get" action="<?= get_site_url()?>">
                    <input type="text" name="s" value="<?= get_search_query() ?>" placeholder="Что будем искать?">
                </form>
                <svg class="Icon Close" viewBox="0 0 22 22">
                    <g fill="none" stroke="#fff" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10">
                        <line x1="2" y1="20" x2="20" y2="2"/>
                        <line x1="20" y1="20" x2="2" y2="2"/>
                    </g>
                </svg>
            </div>

            <div class="Menu-Popup">
                <div class="Menu-Popup-Content">
                    <svg class="Icon Search" viewBox="0 0 22 22">
                        <path d="M21.1,18.9l-6.3-6.3C15.5,11.4,16,10,16,8.5C16,4.4,12.6,1,8.5,1C4.4,1,1,4.4,1,8.5S4.4,16,8.5,16c1.5,0,2.9-0.5,4.1-1.2
	l6.3,6.3c0.3,0.3,0.7,0.4,1.1,0.4s0.8-0.1,1.1-0.4C21.6,20.5,21.6,19.5,21.1,18.9z M4,8.5C4,6,6,4,8.5,4S13,6,13,8.5S11,13,8.5,13
	S4,11,4,8.5z"/>
                    </svg>

                    <input type="search">
                    <svg class="Icon Close" viewBox="0 0 22 22">
                        <g fill="none" stroke="#fff" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10">
                            <line x1="2" y1="20" x2="20" y2="2"/>
                            <line x1="20" y1="20" x2="2" y2="2"/>
                        </g>
                    </svg>

                    <?php wp_nav_menu([
                        'container' => '',
                        'menu_class' => 'Menu-Links',
                        'theme_location' => '',
                        'items_wrap' => '<ul class="%2$s">%3$s</ul>',
                    ]) ?>

                    <ul class="Social-Links">
                        <li><a href="#"><i class="Icons Telegram Gray"></i></a></li>
                        <li><a href="#"><i class="Icons Facebook Gray"></i></a></li>
                        <li><a href="#"><i class="Icons Instagram Gray"></i></a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <header class="header"></header>
