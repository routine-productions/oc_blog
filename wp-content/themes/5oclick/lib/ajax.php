<?php

add_action("wp_ajax_nopriv_ajax_pagination", "ajax_load");
add_action("wp_ajax_ajax_pagination", "ajax_load");

function ajax_load() {
	$result = ["result" => 0];
	ob_start();
	ob_implicit_flush(false);

	$page = !empty($_POST["page"]) ? (int) $_POST["page"] : 1;
	if (!$page) $page = 1;
	$posts = new WP_Query([
		"paged" => (int) $page,
		"posts_per_page" => 6,
		"post__not_in" => exclude_posts(),
		"post_status" => "publish",
	]);

	if($posts->have_posts()) {
		echo "<div class=\"row\">\n";
		$i= 0;
		while ($posts->have_posts()) {
			$posts->the_post();
			get_template_part("parts/card");
			if (++$i % 3 == 0) echo "</div><div class=\"row\">";
		}
		echo "</div>";

		$result = [
			"result" => 1,
			"html" => ob_get_clean(),
		];
	}

	echo json_encode($result, JSON_UNESCAPED_UNICODE);
	die();
}

function exclude_posts() {
	$result = [];

	$posts = new WP_Query([
		"posts_per_page" => 4,
		"orderby" => "post_date",
		"order" => "DESC",
		"post_status" => "publish",
	]);
	if ($posts->have_posts()) {
		foreach ($posts->posts as $post) {
			$result[] = $post->ID;
		};
	}
	
	return $result;
}
