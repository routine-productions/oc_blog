<?php

function exec_the_content($content) {
	if (is_admin() || !is_single()) {
		return preg_replace("~\\[left\\].*?\\[left\\]~isuD", "", $content);
	}

	$tidyConfig = [
		"clean" => true,
		"output-xhtml" => false,
		"show-body-only" => true,
		"indent" => false,
		"wrap" => 0,
	];

	$left = [];
	$content = preg_replace_callback("~\\[left\\](.*?)\\[/left\\]~isuD", function($arr) use(&$left, $tidyConfig) {
		$md5 = md5(mt_rand(0, time()));
		$left[$md5] = (string) tidy_parse_string($arr["1"], $tidyConfig, "utf8");
		return "[left-$md5]".trim(str_replace($left[$md5], "", $arr["1"]));
	}, $content);

	if (!empty($left)) {
		$content = preg_replace_callback("~\\[left\\-([0-9a-f]+)\\]~isuD", function($item) use($left) {
			if (isset($left[$item["1"]])) {
				return "[left]".$left[$item["1"]]."[/left]";
			}
			return "";
		}, $content);
	}

	$content = preg_replace("~(<p([^>]+)?>)(\\s+)?<br(\\s+)?(/)?>~isuD", "$1", $content);

	return $content;
}
add_filter("the_content", "exec_the_content");
