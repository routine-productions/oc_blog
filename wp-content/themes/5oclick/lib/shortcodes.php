<?php

function left_block($attributes, $content) {
	if (!is_admin() && is_single()) {
		return "<span class=\"left-mark\">$content</span>";
	}
	return "";
}
add_shortcode("left", "left_block");
