<?php

function __setup(){
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'html5', array(
		'search-form',
		'gallery',
		'caption',
	) );
	register_nav_menus( array(
		'navigation'  => 'Главное меню'
	) );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head' );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );
	remove_action( 'wp_head', 'wp_generator' );
}
add_action('after_setup_theme', '__setup');

function dateToRussian($date) {
	$month = array("january"=>"января", "february"=>"февраля", "march"=>"марта", "april"=>"апреля", "may"=>"мая", "june"=>"июня", "july"=>"июля", "august"=>"августа", "september"=>"сентября", "october"=>"октября", "november"=>"ноября", "december"=>"декабря");
	$days = array("monday"=>"Понедельник", "tuesday"=>"Вторник", "wednesday"=>"Среда", "thursday"=>"Четверг", "friday"=>"Пятница", "saturday"=>"Суббота", "sunday"=>"Воскресенье");
	return str_replace(array_merge(array_keys($month), array_keys($days)), array_merge($month, $days), strtolower($date));
}

function menu_item_class($classes, $item) {
	return "nav_menu_item" == $item->post_type ? ["category"] : $classes;
}
add_filter('nav_menu_css_class', 'menu_item_class', 10, 2);

function post_in_index($query) {
	if(!is_admin() && $query->is_main_query() && is_home()) {
		$query->set('posts_per_page', '1');
	}
}
add_action('pre_get_posts', 'post_in_index');

function wpb_set_post_views($postID) {
	$count_key = 'wpb_post_views_count';
	$count = (int) get_post_meta($postID, $count_key, true);
	if(!$count){
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, $count);
	}else{
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);

function wpb_track_post_views ($post_id) {
	if ( !is_single() ) return;
	if ( empty ( $post_id) ) {
		global $post;
		$post_id = $post->ID;
	}
	wpb_set_post_views($post_id);
}
add_action('wp_head', 'wpb_track_post_views');

function get_lines($content, $allow_tags = "") {
	$content = preg_replace("~\\[.*?\\][^\\[]+\\[.*?\\]~isuD", "", $content);
	$arr = explode(PHP_EOL, $content);
	$arr = array_map(function($str) use ($allow_tags) {
		$str = trim(strip_tags($str, $allow_tags));
		return mb_strlen($str, "utf-8") ? $str : null;
	}, $arr);

	return array_filter($arr);
}

function except() {
	$result = "";
	$post = get_post();
	if (!empty($post)) {
		$lines = get_lines($post->post_content);
		if (count($lines)) $result = "<p>".implode("</p>\n<p>", array_slice($lines, 0, 1))."</p>";
	}
	return $result;
}
add_action('get_the_excerpt', "except");

function get_card_content($content) {
	$result = "";
	$lines = get_lines($content);
	if (count($lines)) $result = "<p>".implode("</p>\n<p>", array_slice($lines, 0, 1))."</p>";
	return $result;
}

function get_preview_content($content) {
	$result = "";
	$lines = get_lines($content);
	if (count($lines)) $result = "<p>".implode("</p>\n<p>", array_slice($lines, 0, 3))."</p>";
	return $result;
}

function get_image($resize = false) {
	$result = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAyAAAAMgAQMAAADhvpQrAAAAA1BMVEUAAACnej3aAAAAZUlEQVR42u3BMQEAAADCoPVPbQZ/oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHgMO68AAXYDd9gAAAAASUVORK5CYII=";
	$image = get_field("image");
	if (empty($image)) {
		$post = get_post();
		if (!empty($post) && preg_match("~<(img[^>]+)>~isuD", $post->post_content, $m)) {
			if (preg_match("~src=(?:'|\")?([^'\"]+)(?:'|\")?~isuD", $m["1"], $mm)) {
				$result = $mm["1"];
			}
		}
	} else {
		$result = !empty($image["medium"]) ? $image["medium"] : $image["url"];
	}
	return "<div class=\"main-img\" style=\"background-image: url($result)\"></div><img class=\"hidden\" src=\"$result\">";
}

function post_block(array $params) {
	$popularpost = new WP_Query($params);
	while ($popularpost->have_posts()) {
		$popularpost->the_post();
		get_template_part("parts/card");
	}
}

function latest_block(array $params) {
	$params["posts_per_page"]++;
	$first = true;
	$lastposts = new WP_Query($params);
	while ($lastposts->have_posts()) {
		$lastposts->the_post();
		if ($first) {
			$first = false;
			continue;
		}
		get_template_part("parts/card");
	}
}

function get_bages_new($post_id) {
	$last = wp_get_recent_posts([1]);
	if (!empty($last)) {
		$last_id = $last['0']['ID'];
		return !empty($last_id) && $last_id == $post_id ? "<span class=\"bages new\"><span>новый</span></span>" : "";
	}
	return "";
}

function get_read_time($post_id) {
	return "10 минут на чтение";
}

include "lib/content.php";
include "lib/shortcodes.php";
include "lib/ajax.php";
