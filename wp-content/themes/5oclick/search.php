<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage 5oclick2016
 */

get_header();
get_template_part('parts/social-mobile');
get_template_part('parts/index-header');
?>


<?php if (have_posts()) : ?>
    <?php
    // Start the loop.
    while (have_posts()) : the_post(); ?>

        <?php
        get_template_part('parts/index', 'search');
    endwhile;

else :
    get_template_part('content', 'none');
endif;
?>


<?php
if (!is_admin() && !is_home()) {
    get_template_part('parts/navigation');
}

get_footer();
?>
