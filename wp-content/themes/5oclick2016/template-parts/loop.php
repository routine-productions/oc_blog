<?php if ( have_posts() ) : ?>
<div class="loop">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part('template-parts/content'); ?>		
	<?php endwhile; ?>
</div>
<div>
<?php the_posts_pagination($args = array(
								 'show_all' => false,
								 'prev_next' => true,
								 'end_size' => 1,
								 'mid_size' => 1,
								 'before_page_number' => '',
								 'after_page_number' => '',
								 'prev_text' => __('Ранее'),
								 'next_text' => __( 'Позднее' ),
								 'screen_reader_text' => __( '' )
							)
	); ?>
</div>
<?php endif; ?>
