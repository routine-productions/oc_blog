				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> class="e2-note-text e2-text e2-published" >
					<header class="entry-header">
						<div class="entry-meta"></div>
						
						<div class="e2-note-date" title="21 января 2016, 03:32, GMT+03:00"><span class="text-bg"><span class="text-bg__inner"><?php echo dateToRussian( the_date('j F, G:i') ); ?></span></span></div>
						
						<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
					</header>
					<div class="entry-content"><?php the_content("Далее..."); ?></div>
					<footer>
						
						<div class="e2-text-picture">
							<?php $author_email = get_the_author_email(); echo get_avatar($author_email, '100'); ?>
							<!-- <?php the_author(); ?> -->
							<!-- <?php the_author_posts_link(); ?> -->
							<p><?php if(get_the_author_meta('facebook')): ?><a href='<?php the_author_meta('facebook'); ?>' class="author_link"><?php the_author(); ?></a><?php else: the_author_posts_link(); endif; ?>, <?php the_author_description(); ?></p>
						</div>
						
						<?php get_template_part( 'template-parts/likely' ); ?>
						
						<?php get_template_part( 'template-parts/note-tag' ); ?>

						<?php if (is_single()): ?>
						<div class="e2-pages">
							<div class="e2-pages-prev-next">
								<?php
								$prev_post = get_previous_post();
								if (!empty( $prev_post )): ?>
								  <div class="e2-pages-prev"><a href="<?php echo get_permalink( $prev_post->ID ); ?>"><?php echo $prev_post->post_title; ?></a></div>
								<?php endif; ?>
								
								<div class="e2-pages-between">←<span class="e2-keyboard-shortcut">Ctrl</span>→</div>
						
								<?php
								$next_post = get_next_post();
								if (!empty( $next_post )): ?>
								<div class="e2-pages-next"><a href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo $next_post->post_title; ?></a></div>
								<?php endif; ?>
							</div>
						</div>
						<?php endif; ?>
						
						<?php if (!is_single()): ?>
						<div class="e2-section-heading">
							<div class="e2-note-comments-link"><a href="<?php echo get_permalink();?>#comments"><?php
								printf( _nx( '1 комментарий', '%1$s комментариев ', get_comments_number(), 'comments title', 'twentyfifteen' ),
									number_format_i18n( get_comments_number() ), get_the_title() );
								?></a></div>
						</div>
						<?php endif; ?>
						
					</footer>
				</article>
	
			</div>
		</div>
	</div>
</div>

<?php if (is_single()): ?>
<div id="comments" name="comments" class="layout__layout sub-content">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
			<?php
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>
			
			<div class="e2-section-heading">Популярное</div>
			<div class="e2-popular"><?php wpp_get_mostpopular('range="all"'); ?></div>
			
			</div>
			
		</div>
	</div>
</div>
<?php endif; ?>

<div class="container">
	<div class="row">
		<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">