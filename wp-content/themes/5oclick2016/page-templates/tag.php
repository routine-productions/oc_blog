<?php
/**
 * Template Name: Tag template
 * The template for displaying Tag pages
 *
 * @package WordPress
 * @subpackage 5oclick2016
 */

get_header(); ?>
<div class="e2-heading">
	<h2>Теги</h2>
</div>
<div class="e2-tags">
	<?php
	$args = array(
		 'smallest'                  => 8
		,'largest'                   => 22
		,'unit'                      => 'pt'
		,'number'                    => 45
		,'format'                    => 'flat'
		,'separator'                 => "\n"
		,'orderby'                   => 'name'
		,'order'                     => 'ASC'
		,'exclude'                   => null
		,'include'                   => null
		,'topic_count_text_callback' => 'default_topic_count_text'
		,'link'                      => 'view'
		,'taxonomy'                  => 'post_tag'
		,'echo'                      => true
	); 
	wp_tag_cloud( $args );
	?>
</div>
<?php get_footer(); ?>