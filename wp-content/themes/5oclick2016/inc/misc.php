<?php
// Remove default image sizes
function foc_remove_default_image_sizes($sizes) {
    unset( $sizes['medium']);
    unset( $sizes['thumbnail']);
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'foc_remove_default_image_sizes');

function foc_allow_tg_protocol( $protocols ) {
    $protocols[] = 'tg';
    return $protocols;
}

add_filter( 'kses_allowed_protocols', 'foc_allow_tg_protocol' );

// Get years
function foc_getYears($start){
    if ( date('Y') <= $start) {
        return $start;
    } else {
        return $start.' &mdash; '.date('Y');
    }
}
