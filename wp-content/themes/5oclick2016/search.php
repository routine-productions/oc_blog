<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage 5oclick2016
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Результаты поиска по запросу: %s', 'twentyfifteen' ), get_search_query() ); ?></h1>
			</header><!-- .page-header -->

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post(); ?>

				<?php
				/*
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'content', 'search' );
			
			// End the loop.
			endwhile;

			the_posts_pagination($args = array(
									 'show_all' => false,
									 'prev_next' => true,
									 'end_size' => 1,
									 'mid_size' => 1,
									 'before_page_number' => '',
									 'after_page_number' => '',
									 'prev_text' => __('Ранее'),
									 'next_text' => __( 'Позднее' ),
									 'screen_reader_text' => __( '' )
							));

			
			// Previous/next page navigation.
			/*the_posts_pagination( array(
				'prev_text'          => __( 'Предыдущая страница', 'twentyfifteen' ),
				'next_text'          => __( 'Следующая страница', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
			) );*/

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_footer(); ?>
