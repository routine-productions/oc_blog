<?php
/**
 * Template Name: Tag template
 * 
 * The template for displaying Tag pages
 *
 * @package WordPress
 * @subpackage 5oclick2016
 */
get_header(); ?>
<div class="e2-heading">
	<h2>Теги</h2>
</div>
<div class="e2-tags">
	<?php
	$args = array(
		 'smallest'                  => 8
		,'largest'                   => 22
		,'unit'                      => 'pt'
		,'number'                    => 45
		,'format'                    => 'flat'
		,'separator'                 => "\n"
		,'orderby'                   => 'name'
		,'order'                     => 'ASC'
		,'exclude'                   => null
		,'include'                   => null
		,'topic_count_text_callback' => 'default_topic_count_text'
		,'link'                      => 'view'
		,'taxonomy'                  => 'post_tag'
		,'echo'                      => true
	); 
	wp_tag_cloud( $args );
	?>

	<?php
		if (is_tag()) {
		$query = new WP_Query( array( 'tag' => single_tag_title() ) );
	?>
		
		<?php if ( have_posts() ) { ?>
		<div class="loop">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part('template-parts/content'); ?>		
			<?php endwhile; ?>
		</div>
		<?php }?>
		<div>
		<?php the_posts_pagination($args = array(
										 'show_all' => false,
										 'prev_next' => true,
										 'end_size' => 1,
										 'mid_size' => 1,
										 'before_page_number' => '',
										 'after_page_number' => '',
										 'prev_text' => __('Ранее'),
										 'next_text' => __( 'Позднее' ),
										 'screen_reader_text' => __( '' )
									)
		);?>
		</div>
		<?php } ?>
	
</div>
<?php get_footer(); ?>