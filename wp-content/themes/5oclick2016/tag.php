<?php
/**
 * Template Name: Tag template
 * 
 * The template for displaying Tag pages
 *
 * @package WordPress
 * @subpackage 5oclick2016
 */
get_header(); ?>

<div class="e2-notes post">
<?php
	if ( is_category() ) :
		$query = new WP_Query( array( 'category' => single_cat_title() ) );
		get_template_part('template-parts/loop');
	elseif ( is_tag() ) :
		$query = new WP_Query( array( 'tag' => single_tag_title() ) );
		get_template_part('template-parts/loop');
	elseif ( is_author() ) :
		$query = new WP_Query( array( 'author' => get_the_author() ) );
		get_template_part('template-parts/loop');
	else :
	?>
		<div class="e2-heading">
			<h2>Теги</h2>
		</div>
		<div class="e2-tags">
			<?php
			$args = array(
				 'smallest'                  => 8
				,'largest'                   => 22
				,'unit'                      => 'pt'
				,'number'                    => 45
				,'format'                    => 'flat'
				,'separator'                 => "\n"
				,'orderby'                   => 'name'
				,'order'                     => 'ASC'
				,'exclude'                   => null
				,'include'                   => null
				,'topic_count_text_callback' => 'default_topic_count_text'
				,'link'                      => 'view'
				,'taxonomy'                  => 'post_tag'
				,'echo'                      => true
			); 
			wp_tag_cloud( $args );
			?>
		</div>
	<?
	endif;
?>
</div>
<?php get_footer(); ?>
