<div class="spotlight">
<form id="e2-search" class="e2-enterable" action="<?php bloginfo('url'); ?>/" method="get" accept-charset="utf-8">
<div class="e2-search">
  <label class="e2-search-input">
    <input class="text" name="s" id="s" placeholder="Поиск заметок и тэгов" value="<?php echo htmlspecialchars(get_search_query() ? get_search_query() : $searchText ); ?>" type="search">
  </label>
</div>
</form>
</div>