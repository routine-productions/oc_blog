<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head prefix="og: http://ogp.me/ns#">
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="HandheldFriendly" content="true">

	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.png" type="image/x-icon"/>
	
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" type="text/css" media="all"/>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/likely.original.css" type="text/css" media="all"/>
	
	<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,300|Roboto:400,300,300italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/5oclick.ru_bootstrap.min.css" type="text/css" media="all"/>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/5oclick.ru_style.css" type="text/css" media="all"/>

	<?php wp_head(); ?>
		
</head>
<body <?php body_class(); ?>>

    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KN7VSD"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KN7VSD');</script>
    <!-- End Google Tag Manager -->

	<!-- 5oclick cta block -->
	<div class="cta-block">
		<div class="cta-block_header">Кто мы?</div>
		<div class="cta-block_text">Агентство интернет-продаж. Узнайте, чем мы будем вам полезны — скачайте презентацию.</div>
		<div class="cta-block_link">
			<a href="http://www.5oclick.ru/presentation_5oclick_2015.pdf" target="_blank">Скачать</a>
		</div>
	</div>
	<!-- 5oclick cta block -->

	<!-- header from 5oclick.ru -->
	<header class="top" id="bar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-1 col-xs-4 col-sm-1 col-md-1">
					<a href="javascript:void(0);" class="nav-lock" id="nav-lock">Меню</a>
					<div class="navigation" id="navigation">
						<div class="swipe-area hidden-lg hidden-md"></div>
						<ul>
							<li>
								<a href="http://www.5oclick.ru/">Главная</a>
								<ul>
									<li><a href="http://www.5oclick.ru/#example">Примеры продвижения</a></li>
									<li><a href="http://www.5oclick.ru/#tools">Наши инструменты</a></li>
									<li><a href="http://www.5oclick.ru/#opinions">Отзывы и рекомендации</a></li>
									<li><a href="http://www.5oclick.ru/#howitworks">Процесс</a></li>
									<li><a href="http://www.5oclick.ru/#team">Команда</a></li>
									<li><a href="http://www.5oclick.ru/#contacts">Контакты</a></li>
								</ul>
							</li>

						</ul>
						<ul>
							<li><a href="http://partners.5oclick.ru/seo">Партнёрские программы</a></li>
							<li><a href="http://5oclick.ru/lp">Посадочные страницы</a></li>
							<li><a href="http://5oclick.ru/blog/">Блог</a></li>
                            <li><a href="http://5oclick.ru/careers/">Вакансии</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-lg-offset-7 col-xs-8 col-sm-7 col-sm-offset-4 col-md-5 col-md-offset-6">
					<div class="contact">
						<div class="city">Москва</div>
						<span class="tel">+7 (495) 133-72-15</span>
					</div>
					<div class="contact">
						<div class="city">Cанкт-Петербург</div>
						<span class="tel">+7 (812) 903-07-20</span>
					</div>
				</div>
			</div>
		</div>
		<a href="http://5oclick.ru/blog/" class="logo"></a>
	</header>
	<!-- header from 5oclick.ru -->
	
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
				<header class="site-header">
					<?php if ( is_home() ) : ?>
						<h1 class="site-title"><?php bloginfo('name'); ?></h1>
					<?php else : ?>
						<div class="site-title"><?php bloginfo('name'); ?></div>
					<?php endif; ?>

					<div class="site-description"><?php bloginfo('description'); ?></div>

					<?php wp_nav_menu(array('container_class' => 'menu', 'menu_class' => false, 'theme_location' => 'navigation' ) ); ?>
					<div class="site-header-bottom-border"></div>
				</header> <!-- /.site-header -->

				<main class="site-content">
