<?php

if ( ! interface_exists( 'iYoast_License_Manager', false ) ) {

	interface iYoast_License_Manager {

		public function specific_hooks();

		public function setup_auto_updater();

	}

}


if ( ! class_exists( 'Yoast_License_Manager', false ) ) {

	/**
	 * Class Yoast_License_Manager
	 *
	 * @todo Maybe create a license class that contains key and option
	 * @todo Not sure if Yoast_License_Manager is a good name for this class, it's more managing the product (plugin or theme)
	 */
	abstract class Yoast_License_Manager implements iYoast_License_Manager {

		/**
		 * @const VERSION The version number of the License_Manager class
		 */
		const VERSION = 1;

		/**
		 * @var Yoast_License The license
		 */
		protected $product;

		/**
		 * @var string
		 */
		private $license_constant_name = '';

		/**
		 * @var boolean True if license is defined with a constant
		 */
		private $license_constant_is_defined = false;

		/**
		 * @var boolean True if remote license activation just failed
		 */
		private $remote_license_activation_failed = false;

		/**
		 * @var array Array of license related options
		 */
		private $options = array();

		/**
		 * @var string Used to prefix ID's, option names, etc..
		 */
		protected $prefix;

		/**
		 * @var bool Boolean indicating whether this plugin is network activated
		 */
		protected $is_network_activated = false;

		/**
		 * Constructor
		 *
		 * @param Yoast_Product $product
		 */
		public function __construct( Yoast_Product $product ) {

			// Set the license
			$this->product = $product;

			// set prefix
			$this->prefix = sanitize_title_with_dashes( $this->product->get_item_name() . '_', null, 'save' );

			// maybe set license key from constant
			$this->maybe_set_license_key_from_constant();
		}

		/**
		 * Setup hooks
		 *
		 */
		public function setup_hooks() {

			// show admin notice if license is not active
			add_action( 'admin_notices', array( $this, 'display_admin_notices' ) );

			// catch POST requests from license form
			add_action( 'admin_init', array( $this, 'catch_post_request' ) );

			// setup item type (plugin|theme) specific hooks
			$this->specific_hooks();

			// setup the auto updater
			$this->setup_auto_updater();

		}

		/**
		 * Display license specific admin notices, namely:
		 *
		 * - License for the product isn't activated
		 * - External requests are blocked through WP_HTTP_BLOCK_EXTERNAL
		 */
		public function display_admin_notices() {

			if ( ! current_user_can( 'manage_options' ) ) {
				return;
			}

			// show notice if license is invalid
			if ( ! $this->license_is_valid() ) {
				if ( $this->get_license_key() == '' ) {
					$message = '<b>Warning!</b> You didn\'t set your %s license key yet, which means you\'re missing out on updates and support! <a href="%s">Enter your license key</a> or <a href="%s" target="_blank">get a license here</a>.';
				} else {
					$message = '<b>Warning!</b> Your %s license is inactive which means you\'re missing out on updates and support! <a href="%s">Activate your license</a> or <a href="%s" target="_blank">get a license here</a>.';
				}
				?>
				<div class="error">
					<p><?php printf( __( $message, $this->product->get_text_domain() ), $this->product->get_item_name(), $this->product->get_license_page_url(), $this->product->get_tracking_url( 'activate-license-notice' ) ); ?></p>
				</div>
			<?php
			}

			// show notice if external requests are blocked through the WP_HTTP_BLOCK_EXTERNAL constant
			if ( defined( "WP_HTTP_BLOCK_EXTERNAL" ) && WP_HTTP_BLOCK_EXTERNAL === true ) {

				// check if our API endpoint is in the allowed hosts
				$host = parse_url( $this->product->get_api_url(), PHP_URL_HOST );

				if ( ! defined( "WP_ACCESSIBLE_HOSTS" ) || stristr( WP_ACCESSIBLE_HOSTS, $host ) === false ) {
					?>
					<div class="error">
						<p><?php printf( __( '<b>Warning!</b> You\'re blocking external requests which means you won\'t be able to get %s updates. Please add %s to %s.', $this->product->get_text_domain() ), $this->product->get_item_name(), '<strong>' . $host . '</strong>', '<code>WP_ACCESSIBLE_HOSTS</code>' ); ?></p>
					</div>
				<?php
				}

			}
		}

		/**
		 * Set a notice to display in the admin area
		 *
		 * @param string $type    error|updated
		 * @param string $message The message to display
		 */
		protected function set_notice( $message, $success = true ) {
			$css_class = ( $success ) ? 'updated' : 'error';
			add_settings_error( $this->prefix . 'license', 'license-notice', $message, $css_class );
		}

		/**
		 * Remotely activate License
		 * @return boolean True if the license is now activated, false if not
		 */
		public function activate_license() {

			$result = $this->call_license_api( 'activate' );


			if ( $result ) {

				// story expiry date
				if ( isset( $result->expires ) ) {
					$this->set_license_expiry_date( $result->expires );
					$expiry_date = strtotime( $result->expires );
				} else {
					$expiry_date = false;
				}

				// show success notice if license is valid
				if ( $result->license === 'valid' ) {

					$message = sprintf( __( "Your %s license has been activated. ", $this->product->get_text_domain() ), $this->product->get_item_name() );

					// show a custom notice if users have an unlimited license
					if ( $result->license_limit == 0 ) {
						$message .= __( "You have an unlimited license. ", $this->product->get_text_domain() );
					} else {
						$message .= sprintf( __( "You have used %d/%d activations. ", $this->product->get_text_domain() ), $result->site_count, $result->license_limit );
					}

					// add upgrade notice if user has less than 3 activations left
					if ( $result->license_limit > 0 && ( $result->license_limit - $result->site_count ) <= 3 ) {
						$message .= sprintf( __( '<a href="%s">Did you know you can upgrade your license?</a>', $this->product->get_text_domain() ), $this->product->get_tracking_url( 'license-nearing-limit-notice' ) );
						// add extend notice if license is expiring in less than 1 month
					} elseif ( $expiry_date !== false && $expiry_date < strtotime( "+1 month" ) ) {
						$days_left = round( ( $expiry_date - strtotime( "now" ) ) / 86400 );
						$message .= sprintf( __( '<a href="%s">Your license is expiring in %d days, would you like to extend it?</a>', $this->product->get_text_domain() ), $this->product->get_tracking_url( 'license-expiring-notice' ), $days_left );
					}

					$this->set_notice( $message, true );

				} else {

					if ( isset( $result->error ) && $result->error === 'no_activations_left' ) {
						// show notice if user is at their activation limit
						$this->set_notice( sprintf( __( 'You\'ve reached your activation limit. You must <a href="%s">upgrade your license</a> to use it on this site.', $this->product->get_text_domain() ), $this->product->get_tracking_url( 'license-at-limit-notice' ) ), false );
					} elseif ( isset( $result->error ) && $result->error == "expired" ) {
						// show notice if the license is expired
						$this->set_notice( sprintf( __( 'Your license has expired. You must <a href="%s">extend your license</a> in order to use it again.', $this->product->get_text_domain() ), $this->product->get_tracking_url( 'license-expired-notice' ) ), false );
					} else {
	