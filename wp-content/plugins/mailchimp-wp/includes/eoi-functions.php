<?php 


/* ADD ANIMATION CSS TO FRONT-END AND ADMIN PAGES WHEN ENABLED */
add_action( 'fca_eoi_display_lightbox', 'fca_eoi_load_animation_script_popup' );
add_action( 'fca_eoi_render_animation_meta_box', 'fca_eoi_load_animation_script_popup' );

/* REMOVE LINKS FOR USERS WITH FEWER PERMISSIONS THAN EDITOR */
			
function FCA_EOI_remove_admin_bar_link() {
	if (!current_user_can( 'delete_others_pages' ) && function_exists( 'remove_meta_box' )){
		remove_meta_box( 'fca_eoi_dashboard_widget', 'dashboard', 'normal' );	
	}
}

add_action( 'wp_dashboard_setup', 'FCA_EOI_remove_admin_bar_link', 50 );

$fca_eoi_animation_enabled = false; //ONLY ENQUEUE WHEN ITS TURNED ON

function fca_eoi_load_animation_script_popup() {
	global $fca_eoi_animation_enabled;
	if ( $fca_eoi_animation_enabled ) {
		wp_enqueue_style( 'fca_eoi_powerups_animate', FCA_EOI_PLUGIN_URL . '/assets/vendor/animate/animate.css' );
	}
}

function fca_eoi_get_error_texts($id) {
		
	$post_meta = get_post_meta( $id , 'fca_eoi', true );
	$errorTexts = array(
		'field_required' => $post_meta[ 'error_text_field_required' ],
		'invalid_email' => $post_meta[ 'error_text_invalid_email' ],
	);
	
	if (!empty($errorTexts['field_required']) AND  !empty($errorTexts['invalid_email'])) {
		return array(
			'field_required' => $errorTexts['field_required'],
			'invalid_email' => $errorTexts['invalid_email'],
		);
	
	}else{
	
		return array(
			'field_required' => 'Error: This field is required.',
			'invalid_email' => "Error: Please enter a valid email address. For example \"max@domain.com\"."
		);
	}
}

function fca_eoi_get_thanks_mode($id) {
		
	$post_meta = get_post_meta( $id , 'fca_eoi', true );
	
	empty ( $post_meta[ 'thankyou_page_mode' ] ) ? $mode = 'not set' : $mode = $post_meta[ 'thankyou_page_mode' ];
	
	return $mode;
	
}


//CHECK IF USER HAS AN ACTIVE CUSTOM FORM IN ONE OF THEIR OPT INS, OTHERWISE TURN OFF THIS FEATURE
function fca_eoi_set_custom_form_depreciation() {
	
	$optins_using_customform = get_posts( array(
		'post_type' => 'easy-opt-ins',
		'posts_per_page' => -1,
		'post_status' => 'any',
		'meta_key'         => 'fca_eoi_provider',
		'meta_value'       => 'customform',
		
	));
	
	$forms_in_trash = get_posts( array(
		'post_type' => 'easy-opt-ins',
		'posts_per_page' => -1,
		'post_status' => 'trash',
		'meta_key'         => 'fca_eoi_provider',
		'meta_value'       => 'customform',
		
	)); 
	
	if ( count ( $optins_using_customform ) > 0 OR count ( $forms_in_trash ) > 0) {
		update_option ( 'fca_eoi_allow_customform', 'true' );
	} else {
		update_option ( 'fca_eoi_allow_customform', 'false' );
	}
	
}


function fca_eoi_migrate_css() {
	
	require_once FCA_EOI_PLUGIN_DIR . 'powerups/4_new_css/powerup.php';
		
	//CHECK CURRENT SETTING
	$isActive = powerup_new_css_is_active();
	
	if ( !$isActive ) {
				
		$new_css_obj = new EoiNewCssMigration();
		$new_css_obj->migrate( 'old', 'new' );	
		
		powerup_new_css_set_active( TRUE );
	} 

}